# DIY Smart Doorbell using Wemos D1 Mini (with Home Assistant integration)


Smart doorbells are fun and useful to have but they are usually expensive. In this video, I will show you how I upgraded in a non-invasive way a typical doorbell  and connected it to Home Assistant.

[![DIY Smart Doorbell youtube video](https://img.youtube.com/vi/jL0Y6GoEdko/maxresdefault.jpg)](https://youtu.be/jL0Y6GoEdko)

## Components used (Amazon links - affiliate):

* Wemos d1 mini - https://amzn.to/2N7n9zr (US)  or https://amzn.to/3d8ekjB (Canada)
* Mini Voltage Regulator - https://amzn.to/3daQP9q (US) or  https://amzn.to/2BerBtA (Canada)
* 1N4001/4007 Diode - https://amzn.to/30QSizw (US) or  https://amzn.to/3eadLHc (Canada)
* 50V 680uF Capacitor - https://amzn.to/2N1uCA6 (US) or  https://amzn.to/2C5KEqn (Canada)
* Monoprice 3D printer - https://amzn.to/3eb0oqe (US) or  https://amzn.to/2USNQwh (Canada)
* Perfboard - https://amzn.to/3fyaBxq (US) or  https://amzn.to/30L4S3b (Canada)
* Screw Terminals - https://amzn.to/3ecZmKC (US) or  https://amzn.to/2BhTu4c (Canada)

<img alt="Components" src="https://gitlab.com//ontk/diy-smart-doorbell/-/raw/master/components.jpg"  width="640" >



## Steps


I powered the d1 mini using the doorbell AC power supply after I converted it to DC then used a reed switch connected to D5 to detect  the doorbell chime as shown in the schematic below.

<img alt="Components" src="https://gitlab.com//ontk/diy-smart-doorbell/-/raw/master/schematic.jpg"  width="800" >

Most doorbells are powered by a 21 volt AC current. To get a DC current, I first passed it by a diode and then added a 680micro farad capacitor to smooth the output. This is called a half bridge rectifier. Last step was to add a buck converter to drop the voltage down to 5v.

Now that the circuit is ready, it was time to put it all together. I used a 3x7 perfboard and mounted all components according to the schematics. I tested everything to make sure the 21 volt AC current  is converted into a 5v direct current. Once everything looked good, I cut the extra perfboard area.

Next, I printed a small case to easily mount it: https://www.thingiverse.com/thing:1848391

Back at the laptop, I connected the D1 mini to load Tasmota. Next, I followed a typical Tasmota setup to connect it to my network. Once the device was connected, I updated all the settings though the console. You can find the full [command line here](https://gitlab.com/ontk/diy-smart-doorbell/-/raw/master/tasmota_command_line). Just make sure you update the MQTT IP address and the credentials.

Next, it was time for the integration.  

<img alt="Components" src="https://gitlab.com//ontk/diy-smart-doorbell/-/raw/master/chime.jpg"  width="800" >


Before I started, I disconnected the power. Attaching the device to the doorbell was easy:  (1) I removed the cover, (2) attached the device to the wall using screws, (3) connected the power wires, (4) attached the reed switch using double sided tape (5) and put back the cover.

Back in home assistant, I opened the configuration file and [added a new MQTT sensor](https://gitlab.com/ontk/diy-smart-doorbell/-/raw/master/home_assistant_code), saved and restarted. The code is in the description. After I saved, I restarted home assistant.

This doorbell sensor can be used  to trigger alerts or any other Home Assistant automation!

I configured it so it sends me an alert and shows me a stream from my security camera on the TV when someone is at the door.

I hope you found this useful! 

Thank you for reading and I will see you in the next one.

